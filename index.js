const express=require('express')

const logger = require('morgan');

app=express()

app.use(express.static(__dirname + '/public', { maxAge: '7d', lastModified: true }));
app.use(logger('combined'))

app.get('/',(req,res)=>{
    res.render('/index.html')
})

bodyParser=require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))

/*
 * Wait for while
 */
app.use((req, res, next) => {setTimeout(() => next(), 3000)})

app.post('/discover',(req,res)=>{
    console.log(req.body)

    res.json({
        msg:'Server received your request',
	number: 22,
	good: true
    })
})

app.listen(4004,()=>{
    console.log('App is available on port 4004')
})
